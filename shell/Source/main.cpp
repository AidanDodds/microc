/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include <stdio.h>
#include <string.h>
#include "../../microc/source/microc.h"

char buffer[ 1024 ];

/**

 */
int main( void )
{
    char *ptr = buffer;
    int line = 0;

    while ( true )
    {
        printf( "%02u. ", line );
        if ( gets( ptr ) != ptr )
            break;

        int len = strlen( ptr );
        ptr[   len ] = '\n';
        ptr[ ++len ] = '\0';

        if ( len == 1 )
        {
            //
            line = 0;

            // no input so quit now
            if ( ptr == buffer )
                break;
            

            // compile
            if (! microc::compile( buffer ) )
            {
                const char *error = microc::getError( );
                printf( "Error: %s\n", error );
            }
            
            // add a new line for good mesure
            printf( "\n" );

            // reset
            ptr = buffer;
            continue;
        }
        else
        {
            // increment
            ptr += len;
            line++;
        }
    }

    return 0;
}
