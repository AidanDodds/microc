/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */

namespace microc
{

    /**
        Structure storing a compiled program
     */
    struct sProgram
    {
        char *data;
    };

    /**
        Compile a string into bytecode
     */
    extern
    bool compile( char *source /*, sProgram *prog */ );

    /**
        Get the last error
     */
    extern
    const char *getError( void );


    /**
        Execute a function
     */
/*    extern bool execute( sProgram *prog, char *function, params, result ); */

};