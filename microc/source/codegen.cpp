/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "assert.h"
#include "codegen.h"
#include "global.h"
#include "ast.h"
#include "stack.h"
#include "symtab.h"
#include "error.h"
#include "strtab.h"

#include <stdarg.h>
#include <stdio.h>

/*
    XXX: Idea to pipe assembly string to code gen back end
         or just treat this as an IR generator. ???
 */

/**
    Code generator
 */
namespace codegen
{

    /**
        Forward declate common visisors
     */
    static bool visit_exp       ( sASTNode *n );
    static bool visit_statement ( sASTNode *n );

    /**
        Output some data
     */
    static
    void emit( const char *str, ... )
    {
        va_list args;
        va_start( args, str );
        vprintf( str, args );
        putchar( '\n' );
    }

    /**
        Assignment
     */
    bool visit_exp_assign( sASTNode *n )
    {
        assert( n->token->type == eType::EQU );
        /* find the variable we are assigning to */
        sASTNode *var = symtab::find( n->child[0]->token );
        if ( var == nullptr )
            return error::complain( "use of unknown symbol" );
        /* evaluate the RHS expression */
        if (! visit_exp( n->child[1] ) )
            return false;
                
        emit( "mov sym:%d r%d", 0, 0 );

        return true;
    }

    /**
        Addition
     */
    bool visit_exp_add( sASTNode *n )
    {
        assert( n->token->type == eType::ADD );
        if (! visit_exp( n->child[0] ) )
            return false;
        if (! visit_exp( n->child[1] ) )
            return false;
        
        emit( "add r%d r%d", 0, 0 );

        return true;
    }

    /**
        Multiply
     */
    bool visit_exp_mul( sASTNode *n )
    {
        assert( n->token->type == eType::MUL );
        if (! visit_exp( n->child[0] ) )
            return false;
        if (! visit_exp( n->child[1] ) )
            return false;

        emit( "mul r%d r%d", 0, 0 );

        return true;
    }

    /**
        Symbol / variable as expression
     */
    bool visit_exp_symbol( sASTNode *n )
    {
        assert( n->token->type == eType::SYM );
        /* find the variable we are reading from */
        sASTNode *var = symtab::find( n->token );
        if ( var == nullptr )
            return error::complain( "use of unknown symbol" );

        emit( "mov r%d sym:%d", 0 );

        return true;
    }
    
    /**
        Integer literal
     */
    bool visit_exp_integer_literal( sASTNode *n )
    {
        assert( n->token->type == eType::INT );
        
        emit( "load r%d 0x%x", 0, n->token->value );

        return true;
    }

    /** 
        Function call
     */
    bool visit_exp_func( sASTNode *n )
    {
        assert( n->token->type == eType::FUNC );

        /*XXX: save all live registers */
        /*     or evaluate all call functions first in an expression hierarchy  */

        /* find function with name n->child[0] */
        sASTNode *decl = symtab::find( n->child[0]->token );
        if ( decl == nullptr )
            return error::complain( "Unknown function" );

        /* args */
        for ( int i=1; i<n->child.size( ); i++ )
        {
            /* get argument */
            sASTNode *arg = n->child[i];

            /* translate this argument */
            if (! visit_exp( arg ) )
                return false;

            emit( "push r%d" );
        }

        emit( "call sym:%d", 0 );

        /* we need to store the return value */
        emit( "pop r%d", 0 );

        return true;
    }

    /**
        Visit expression statment
     */
    bool visit_exp( sASTNode *n )
    {
        switch ( n->token->type )
        {
        case ( '=' ):
            return visit_exp_assign( n );

        case ( '+' ):
            return visit_exp_add( n );

        case ( '*' ):
            return visit_exp_mul( n );

        case ( eType::FUNC ):
            return visit_exp_func( n );

        case ( eType::INT ):
            return visit_exp_integer_literal( n );

        case ( eType::SYM ):
            return visit_exp_symbol( n );

        case ( eType::EXP ):
            return visit_exp( n->child[0] );

        default:
            return error::complain( "Error in expression" );
        };
    }

    /**
        Visit variable decl
     */
    bool visit_var_decl( sASTNode *n )
    {
        assert( n->token->type == eType::VAR_DECL );
        
        /* add this to the symbol table */
        symtab::add( n );

        /* visit any assignments */
        sASTNode *exp = n->child[2];
        if ( exp != nullptr )
        {
            if ( exp->token->type == eType::EXP )
                if (! visit_exp( exp ) )
                    return false;

            emit( "mov sym:%d r%d", 0, 0 );
        }

        /* success */
        return true;
    }

    /** 
        Visit if statment
     */
    bool visit_if( sASTNode *n )
    {
        /**
            child[0]    -> expression
            child[1]    -> if body
            child[2]    -> else body
         */
        assert( n->token->type == eType::IF );

        bool hasElse = (n->child.size( ) == 3);

        /* evaluate condition */
        if (! visit_exp( n->child[0] ) )
            return false;
        /* test condition == 0 */
        emit( "equ r%d, 0", 0 );
        /* skip over if body */
        emit( "cjmp .label1" );

        /* if body block */
        if (! visit_statement( n->child[1] ) )
            return false;

        /* jump over else body */
        if ( hasElse )
            emit( "jmp .label2" );

        /* end of if body */
        emit( ".label1" );

        /* start of else body */
        if ( hasElse )
        {
            /* else body block */
            if (! visit_statement( n->child[2] ) )
                return false;

            emit( ".label2" );
        }

        /* success */
        return true;
    }

    /**
        Visit while statement
     */
    bool visit_while( sASTNode *n )
    {
        /**
            child[0]    -> expression
            child[1]    -> while body
         */
        assert( n->token->type == eType::WHILE );

        emit( ".top" );

        /* evaluate condition */
        if (! visit_exp( n->child[0] ) )
            return false;

        emit( "equ r%d, 0", 0 );
        emit( "cjmp .end" );

        if (! visit_statement( n->child[1] ) )
            return false;

        emit( "jmp .top" );
        emit( ".end" );

        return true;
    }

    /**
        Visit return statment
     */
    bool visit_ret( sASTNode *n )
    {
        /**
            child[0]    -> return value
         */
        assert( n->token->type == eType::RET );

        if ( n->child.size() > 0 )
        {
            if (! visit_exp( n->child[0] ) )
                return false;

            /* push return value */
            emit( "push r%d", 0 );
        }
        emit( "ret" );

        return true;
    }

    /**
        Visit statment types
     */
    bool visit_statement( sASTNode *n )
    {
        switch ( n->token->type )
        {

        /* visit variable declaration */
        case ( eType::VAR_DECL ):
            if (! visit_var_decl( n ) )
                return false;
            break;

        /* visit expression */
        case ( eType::EXP ):
            if (! visit_exp( n ) )
                return false;
            break;

        /* visit if statement */
        case ( eType::IF ):
            if (! visit_if( n ) )
                return false;
            break;

        /* visit while statment */
        case ( eType::WHILE ):
            if (! visit_while( n ) )
                return false;
            break;

        /* visit return statement */
        case ( eType::RET ):
            if (! visit_ret( n ) )
                return false;
            break;

        /* block of statments */
        case ( eType::BLOCK ):
            {
                /* enter new scope level */
                symtab::sScope newScope;
                /* visit all children */
                for ( int i=0; i<n->child.size(); i++ )
                {
                    sASTNode *stm = n->child[i];
                    if (! visit_statement( stm ) )
                        return false;
                }
            }
            break;

        /* unknown statment */
        default:
            return error::complain( "Expecting statment" );
        }

        /* success */
        return true;
    }

    /**
        Visit function argument list
     */
    bool visit_function_arg( sASTNode *n )
    {
        assert( n->token->type == eType::FUNC_ARG );

        /* add argument to the symbol table */
        symtab::add( n );

        /* success */
        return true;
    }

    /**
        Visit function definition
     */
    bool visit_function( sASTNode *n )
    {
        assert( n->token->type == eType::FUNC_DECL );
        const char *name = strtab::lookup( n->child[1]->token );
        int index = 0;
        
        /* enter new scope level */
        symtab::sScope newScope;

        emit( ".function %s", name );

        /* visit return type */
        n->child[ 0 ];

        /* visit arguments */
        n->child[ 1 ];
        
        /* visit all arguments */
        index = 2;
        while ( n->child[ index ]->token->type == eType::FUNC_ARG )
        {
            visit_function_arg( n->child[ index ] );
            index++;
        }

        /* allocate stack space ? scratch space ? */
        emit( "alloc #%d", index-2 );

        /* visit function body as block */
        sASTNode *body = n->child[ index ];
        if (! visit_statement( body ) )
            return false;
        
        /* blank space for assembly */
        emit( "" );
        /* success */
        return true;
    }

};

/**
    Emit bytecode for the AST
 */
extern
bool codegen::consume( void )
{
    /* get the root of the AST tree */
    sASTNode *node = ast::getRoot( );
    assert( node->token->type == eType::ROOT );
    
    /* loop over all functions in the tree and add to symbol table */
    for ( int i=0; i<node->child.size(); i++ )
    {
        sASTNode *func = node->child[i];
        if ( func->token->type == eType::FUNC_DECL )
        {
            symtab::add( func );
        }
    }

    /* now codegen all functions in the AST */
    for ( int i=0; i<node->child.size(); i++ )
    {
        sASTNode *func = node->child[i];
        if ( func->token->type == eType::FUNC_DECL )
        {
            /* translate a function */
            if (! visit_function( func ) )
                return false;
        }
    }

    /* success */
    return true;
}

/**
    Clear all codegen state
 */
extern
void codegen::reset( void )
{
    /* reset register number */
    
}
