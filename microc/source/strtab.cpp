/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "global.h"
#include "strtab.h"

namespace strtab
{

    /**
        32kb string table
     */
    static char _stringTable[1024*32];

    /**
        Allocation pointer
     */
    static int  _head=0;
};

/**
    provide a string and return a copy of it
 */
extern
int strtab::push( char *tail, char *head )
{
    //
    int index = _head;
    // 
    char *x = _stringTable + _head;
    // copy over the input string
    int i=0;
    for ( ; ; i++ )
    {
        if ( (tail+i) == head )
            break;
        x[i] = tail[i];
    }
    // insert end of line char
    x[i] = '\0';
    // advance the head
    _head += (i + 1);
    // return the string we pushed
    return index;
}

/**
    Lookup a string via index
 */
extern
char* strtab::lookup( int index )
{
    return _stringTable + index;
}

/**
    Clear all stored strings
 */
extern
void strtab::reset( void )
{
    _head = 0;
}


/**
    Find a string via token
    */
extern
char *strtab::lookup( sToken *tok )
{
    assert( tok->type == eType::SYM );
    return _stringTable + tok->value;
}