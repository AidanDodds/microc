/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once

#include <string.h>

#include "symtab.h"
#include "strtab.h"
#include "error.h"

/**
    Symbol table
 */
namespace symtab
{
    sSymbol symtab[ 1024 ];
    int scope = 0;
    int list[ 64 ];

    /**
        Find the name of a declared type
     */
    static char * getNodeName( sASTNode *node )
    {
        switch ( node->token->type )
        {
        case ( eType::VAR_DECL  ):
            {
                int ix = node->child[1]->token->value;
                return strtab::lookup( ix );
            }
            break;

        case ( eType::FUNC_DECL ):
            {
                int ix = node->child[1]->token->value;
                return strtab::lookup( ix );
            }
            break;

        case ( eType::FUNC_ARG ):
            {
                int ix = node->child[1]->token->value;
                return strtab::lookup( ix );
            }
            break;

        default:
            assert(! "bad decl type" );
        }

        /* fail */
        return false;
    }

};

/**
    Add a node to the symbol table
 */
extern bool symtab::add( sASTNode *node )
{
    assert( node != nullptr );
//    assert ( node->token->type == eType::SYM );
    
    /* find the name of this decl */
    char *name = getNodeName( node );
    if ( name == nullptr )
        return false;

    /* check for name clash */
    if ( find( name ) != nullptr )
        return error::complain( "duplicate name conflict" );
        
    /* get new symbol table entry */
    int &head = list[ scope ];
    sSymbol &symbol = symtab[ head++ ];
    
    /* add this symbol to the list */
    symbol.name = name;
    symbol.node = node;

    return true;
}

/**
    Find a decl from a symbol
 */
extern sASTNode *symtab::find( const char *name )
{
    assert( name != nullptr );

    /* search backwards from head */
    int head = list[ scope ] - 1;
    
    for ( int i=head; i>=0; --i )
    {
        sSymbol &sym = symtab[ i ];

        if ( strcmp( sym.name, name ) == 0 )
            return sym.node;
    }

    return nullptr;
}

/**
    Find a decl from a symbol
 */
extern sASTNode *symtab::find( const sToken *tok )
{
    /*  */
    assert( tok->type == eType::SYM );
    const char *name = strtab::lookup( tok->value );

    /* search backwards from head */
    int head = list[ scope ] - 1;
    
    for ( int i=head; i>=0; --i )
    {
        sSymbol &sym = symtab[ i ];

        if ( strcmp( sym.name, name ) == 0 )
            return sym.node;
    }

    return nullptr;
}

/**
 
 */
extern void symtab::reset( void )
{
    scope = 0;
    list[ scope ] = 0;
}

/**
    Enter a level of scope
 */
void symtab::enter( void )
{
    list[ scope+1 ] = list[ scope ];
    scope++;
}

/**
    Leave a level of scope
 */
void symtab::leave( void )
{
    scope--;
}