/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "stack.h"

/**
    Token type codes
 */
namespace eType { enum
{
    END			= 0x00,

    ADD			= '+',
    SUB			= '-',
    MUL			= '*',
    DIV			= '/',
    EQU			= '=',

    AND			= '&',
    NOT			= '!',
    XOR			= '^',

    LPAREN		= '(',
    RPAREN		= ')',
    LARRAY		= '[',
    RARRAY		= ']',
    COMMA		= ',',
    SEMICOLON	= ';',
    LBRACE		= '{',
    RBRACE		= '}',

    SYM			= 0x80,
    INT			,
    EXP			,
    DEREF		,
    ADDRESS		,
    FUNC		,
    IF			,
    WHILE		,
    BLOCK		,
    RET			,
    VAR_DECL	,
    FUNC_ARG	,
    FUNC_DECL	,
    ROOT        , /* program root */

    KEY_VOID	= 0x1000,
    KEY_INT		,
    KEY_WORD	,
    KEY_BYTE	,
    KEY_RETURN	,
    KEY_BREAK	,
    KEY_WHILE	,
    KEY_FOR		,
    KEY_CONTINUE,
    KEY_NULL	,
    KEY_IF		,
    KEY_ELSE	,
    KEY_DO		,
    KEY_LOOP	,

}; }
typedef unsigned int eType_t;

/**
    Token type codes
 */
struct sToken
{
    sToken( )
        : type ( 0 )
        , value( 0 )
        , line ( 0 )
    {}

    /* token type */
    eType_t type;

    /* token value */
    int     value;

    /* source code line number */
    int		line;
};

/**
    Token type codes
 */
struct sASTNode
{
    /* Constructor */
    sASTNode( void )
        : token( 0 )
        , child( )
    {}
    
    /* Base token type */
    sToken * token;

    /* Token type codes */
    cStack<sASTNode,8> child;
};

/**
    A symbol in the AST
 */
struct sSymbol
{
    /*  */
    sASTNode *node;

    /*  */
    const char *name;
};