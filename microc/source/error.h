/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once

namespace error
{
    
    /**
        Flag an error state with a message
     */
    bool complain( const char *str, ... );


    /**
        Get the last error message
     */
    const char *getErrorMsg( void );

    /**
        Check if program has failed
     */
    bool hasFailed( void );

    /**
        Clear all error state
     */
    void reset( void );

};