/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "error.h"


namespace error
{
    /**
        The last error message

XXX:    Perhaps a stack of error messages would be better?
     */
    static const char *lastError = nullptr;
};

/**
    Set an error state with a message
 */
bool error::complain( const char *str, ... )
{
    lastError = str;
    return false;
}

/**
    Get the last error message
 */
const char *error::getErrorMsg( void )
{
    return lastError;
}

/**
    Check if we have failed
 */
bool error::hasFailed( void )
{
    return lastError != nullptr;
}

/**
    Reset all error state
 */
void error::reset( void )
{
    lastError = nullptr;
}