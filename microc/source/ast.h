/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "global.h"

/**
    Abstract Syntax Tree
 */
namespace ast
{
    
    /**
        Pack nodes on the ast stack (reduce)
     */
    extern void pack( sToken *tok, int numChildren );

    /**
        Push a token onto the AST stack (shift)
     */
    extern void push( sToken *tok );

    /**
XXX:    Walk the AST stack (debug)
     */
    extern void debugDump( void );

    /**
        Clear the AST stack
     */
    extern void reset( void );

    /**
        Get the root of the AST tree
     */
    extern sASTNode *getRoot( void );

};