/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "global.h"

/**
    Datastructure allocator
 */
namespace alloc
{
    /**
        Allocate a token
     */
    extern sToken* token( const eType_t type );
    
    /**
        Allocate a token
     */
    extern sToken* token( const eType_t type, const int value );
    
    /**
        Allocate an AST node
     */
    extern sASTNode* astnode( sToken * token );
    
    /**
        Release all allocated types
     */
    extern void reset( void );
};
