/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "allocator.h"

namespace alloc
{
    const int nItems = 1024 * 8;

    static sToken   token_list  [ nItems ];
    static int      token_head   = 0;

    static sASTNode astNode_list[ nItems ];
    static int      astNode_head = 0;

    static void clearMem( void *dst, int size );

};

//
//
//
extern
sToken * alloc::token( const eType_t type )
{
    assert( token_head < nItems );
    sToken & tok = token_list[ token_head++ ];
    tok.type  = type;
    tok.line  = 0;
    tok.value = 0;
    return &tok;
}

//
//
//
extern
sToken * alloc::token( const eType_t type, const int value )
{
    assert( token_head < nItems );
    sToken & tok = token_list[ token_head++ ];
    tok.type  = type;
    tok.line  = 0;
    tok.value = value;
    return &tok;
}

//
//
//
extern
sASTNode * alloc::astnode( sToken * token )
{
    assert( astNode_head < nItems );
    sASTNode & node = astNode_list[ astNode_head++ ];
    node.token = token;
    return &node;
}

//
//
//
extern
void alloc::reset( void )
{
      token_head = 0;
    astNode_head = 0;

    clearMem(   token_list, sizeof( token_list ) );
    clearMem( astNode_list, sizeof( token_list ) );
}

//
//
//
static
void alloc::clearMem( void *dst, int size )
{
    for ( int i=0; i<size; i++ ) ((char*)dst)[i] = 0;
}
