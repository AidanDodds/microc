/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "global.h"

/**
    String Table
 */
namespace strtab
{
    /**
        Add a character sequence to the string table
     */
    extern int push( char *tail, char *head );

    /**
        Find a string via index
     */
    extern char *lookup( int value );
    
    /**
        Find a string via token
     */
    extern char *lookup( sToken *tok );

    /**
        Clear the string table
     */
    extern void reset( void );
};
