/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include <stdio.h>

#include "ast.h"
#include "global.h"
#include "strtab.h"
#include "assert.h"
#include "allocator.h"

/**
    Abstract syntax tree
 */
namespace ast
{
    static cStack<sASTNode,128> aStack;

    static void dump_token( sToken * tok );
    static void walk__    ( sASTNode * node, int level );

};

/**
    Get the root of the AST tree
 */
extern
sASTNode* ast::getRoot( void )
{
    assert( aStack.size() > 0 );
    return aStack[0];
}

//
//
//
extern
void ast::pack( sToken * tok, int num )
{
    // make sure we have enough items to pop as children
    assert( tok != nullptr );
    assert( aStack.size() >= num );

    // create a new AST node
    sASTNode *node = alloc::astnode( tok );
    assert( node != nullptr );

    // pack children into this node backwards
    for ( int i=num-1; i>=0; --i )
        node->child.push( aStack.peek( i ) );
    aStack.discard( num );

    // add this item back onto the ast stack
    aStack.push( node );
}

//
//
//
extern
void ast::push( sToken * tok )
{
    assert( tok != nullptr );
    aStack.push( alloc::astnode( tok ) );
}

//
//
//
static
void ast::dump_token( sToken * tok )
{
    assert( tok != nullptr );

    switch ( tok->type )
    {
    case ( eType::INT     ):	printf( "#%d", tok->value );		break;
    case ( eType::DEREF   ):	printf( "<deref>", tok->value );	break;
    case ( eType::ADDRESS ):	printf( "<addr>", tok->value );		break;
    case ( eType::FUNC    ):	printf( "<func>" );					break;
    case ( eType::EXP     ):	printf( "<exp>" );					break;
    case ( eType::BLOCK   ):	printf( "<block>" );				break;
    case ( eType::IF      ):	printf( "<if>" );					break;
    case ( eType::RET     ):	printf( "<ret>" );					break;
    case ( eType::WHILE     ):	printf( "<while>" );				break;

    case ( eType::ROOT     ):	printf( "<root>" );			    	break;

    case ( eType::VAR_DECL  ):	printf( "<vardecl>" );				break;
    case ( eType::FUNC_DECL ):	printf( "<funcdecl>" );				break;
    case ( eType::FUNC_ARG ):   printf( "<funcarg>" );              break;
        
    case ( eType::KEY_VOID  ):	printf( "<type:void>" );			break;
    case ( eType::KEY_INT  ):	printf( "<type:int>" );				break;
    case ( eType::KEY_WORD ):	printf( "<type:word>" );			break;
    case ( eType::KEY_BYTE ):	printf( "<type:byte>" );			break;

    case ( eType::SYM ):
        {
            char *name = strtab::lookup( tok->value );
            printf( "$%s", name );
        }
        break;

    default:
        if ( tok->type < 127 )
            printf( "'%c'", tok->type );
        else
            printf( "<%d>",tok->type );
    }
    putchar( '\n' );
}

//
//
//
static
void ast::walk__( sASTNode * node, int level )
{
    for ( int i=0; i<level; i++ )
        printf( ". " );
    putchar( '.' );

    dump_token( node->token );

    for ( int i=0; node->child[i] != nullptr; i++ )
        walk__( node->child[i], level+1 );
}

//
// 
//
extern
void ast::debugDump( void )
{
    for ( int i=0; i<aStack.size( ); i++ )
    {
        walk__( aStack[i], 0 );
        printf( "\n" );
    }
}

//
//
//
extern
void ast::reset( void )
{
    aStack.clear( );
}
