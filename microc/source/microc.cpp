/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */

#include "microc.h"

#include "global.h"
#include "lexer.h"
#include "parser.h"
#include "ast.h"
#include "codegen.h"
#include "allocator.h"
#include "strtab.h"
#include "error.h"
#include "strtab.h"
#include "symtab.h"

/**
    Compile a string into bytecode
 */
extern
bool microc::compile( char *source )
{
    /* reset everything we will use */
    alloc  ::reset( );
    lexer  ::reset( );
    parser ::reset( );
    ast    ::reset( );
    strtab ::reset( );
    codegen::reset( );
    error  ::reset( );
    symtab ::reset( );

    /* lexical analysis */
    if (! lexer ::consume( source ) )
        return false;

    /* syntax analysis */
    if (! parser::consume( ) )
        return false;

    /* debug print the ast */
    ast::debugDump( );

    /* semantic analysis and bytecode gen */
    if (! codegen::consume( ) )
        return false;

    return true;
}

/**
    Get the last error message
 */
extern
const char *microc::getError( void )
{
    return error::getErrorMsg( );
}