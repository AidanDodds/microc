/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "global.h"

/**
    Symbol table
 */
namespace symtab
{

    /**
        Add a node to the symbol table
     */
    extern bool add( sASTNode *node );

    /**
        Find a decl from a symbol name
     */
    extern sASTNode *find( const char *name );
    
    /**
        Find a decl from a symbol token
     */
    extern sASTNode *find( const sToken *tok );

    /**
        Enter a level of scope
     */
    void enter( void );

    /**
        Leave a level of scope
     */
    void leave( void );

    /**
        Clear symbol table
     */
    void reset( void );

    /**
        
     */
    struct sScope
    {
         sScope( void )   { symtab::enter( ); }
        ~sScope( )        { symtab::leave( ); };
    };

};
