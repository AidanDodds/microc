/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include <string>
#include <queue>

#include "assert.h"
#include "global.h"
#include "lexer.h"
#include "strtab.h"
#include "allocator.h"
#include "error.h"

namespace lexer
{
    // data
    static int _tail        = 0;
    static int _lineCounter = 0;
    static cStack<sToken,1024> _lStack;

    // internal
    static sToken * pushToken      ( eType_t type, int val = 0 );
    static bool     isAlpha        ( char num );
    static bool     isNumeric      ( char num );
    static bool     munchWhitespace( char * &stream );
    static bool     strcompare     ( char *s, char *e, const char *key );
    static eType_t  keywordMatch   ( char *start, char *end );
    
    static sToken * munchSymbol    ( char* &head );
    static sToken * munchLiteral   ( char* &head );
    static sToken * munchSpecial   ( char* &stream );

    // interface
    extern sToken * pop            ( void  );
    extern sToken * peek           ( int i );
    extern void     clear          ( void  );
    extern eType_t  peekType       ( int lookAhead );
    extern bool     consume        ( char *head );

    struct sSymbol
    {
        const char *name;

        eType_t type;
    };

    static const sSymbol keywords[] =
    {
        { "void"    , eType::KEY_VOID     },
        { "int"		, eType::KEY_INT      },
//		{ "word"	, eType::KEY_WORD     },
//		{ "byte"	, eType::KEY_BYTE     },
        { "return"  , eType::KEY_RETURN   },
        { "break"	, eType::KEY_BREAK    },
        { "while"   , eType::KEY_WHILE    },
        { "for"		, eType::KEY_FOR      },
        { "continue", eType::KEY_CONTINUE },
//		{ "null"	, eType::KEY_NULL     },
        { "if"		, eType::KEY_IF       },
        { "else"	, eType::KEY_ELSE     },
        { "do"		, eType::KEY_DO       }
    };
    const int nItems = sizeof( keywords ) / sizeof( sSymbol );
};

// push a token onto the lexer queue
static
sToken* lexer::pushToken( eType_t type, int val )
{
    sToken *tok = alloc::token( type );
    assert( tok != nullptr );
    memset( tok, 0, sizeof( sToken ) );
    
    tok->type  = type;
    tok->value = val;
    tok->line  = _lineCounter;
    // push onto the stack
    _lStack.push( tok );

    return tok;
}

// pop next token from the lexer stack
extern
sToken * lexer::pop( void )
{
    assert( _lStack.size() > 0 );
    assert( _tail < _lStack.size() );

    sToken * tok = _lStack[ _tail ];
    assert( tok != nullptr );

    if ( tok->type != eType::END )
        _tail++;

    return tok;
}

// peek next token from the lexer queue
extern
sToken * lexer::peek( int i )
{
    i += _tail;

    assert( i >= 0 );
    assert( i < _lStack.size( ) );
    return _lStack[ i ];
}

// clear all parse nodes
extern
void lexer::clear( void )
{
    _tail        = 0;
    _lineCounter = 0;

    _lStack.clear( );
}

// 
extern
eType_t lexer::peekType( int lookAhead )
{
    return peek( lookAhead )->type;
}

// is a character an alphabet character
static
bool lexer::isAlpha( char num )
{
    bool ret = false;
    ret |= (num >= 'a' && num <= 'z' );
    ret |= (num >= 'A' && num <= 'Z' );
    ret |= (num == '_');
    return ret;
}

// is a character purely numeric
static
bool lexer::isNumeric( char num )
{
    bool ret = false;
    ret |= (num >= '0' && num <= '9' );
    return ret;
}

// skip over all white space in our path
static
bool lexer::munchWhitespace( char * &stream )
{
    char *start = stream;
    //
    for ( ;; stream++ )
    {
        char num = *stream;
        
        bool x  = (num == ' ' );
             x |= (num == '\t');
             x |= (num == '\n');
             x |= (num == '\r');

        // increment the line counter
        _lineCounter += ( num == '\n' );

        if ( !x )
            return (start!=stream);
    }
}

// s   = start
// e   = end
// key = keyword
static
bool lexer::strcompare( char *s, char *e, const char *key )
{
    for ( ; ; s++, key++ )
    {
        // keyword matches
        if ( *key == '\0' )
            if ( s == e )
                return true;

        // end of symbol to match
        if ( s > e )
            return false;

        // symbol and keyword dont match
        if ( *s != *key )
            return false;
    }
}

// match a token to a keyword
static
eType_t lexer::keywordMatch( char *start, char *end )
{
    for ( int i=0; i<nItems; i++ )
    {
        if (! strcompare( start, end, keywords[i].name ) )
            continue;
        return keywords[i].type;
    }
 
    return eType::END;
}

/**
    Munch a symbol or keyword
 */
static
sToken* lexer::munchSymbol( char* &head )
{
    // ensure that alpha always comes first
    if (! isAlpha( *head ) )
        return nullptr;

    // try to match as many alpha numerics a possible
    char *tail = head;
    for ( ;*head != '\0'; head++ )
    {
        if ( isAlpha  ( *head ) ) continue;
        if ( isNumeric( *head ) ) continue;
        break;
    }

    // if we have not read any characters
    if ( head == tail )
        return nullptr;

    // can this symbol be elevated to a keyword
    eType_t type = keywordMatch( tail, head );
    int val  = 0;
    // its not a keyword so add to string table
    if ( type == eType::END )
    {
        type = eType::SYM;
        val  = strtab::push( tail, head );
    }
    return pushToken( type, val );
}

/**
    Munch a numeric literal
 */
static
sToken* lexer::munchLiteral( char* &head )
{
    if (! isNumeric( *head ) )
        return nullptr;

    char *tail  = head;
    int   value = 0;
    for ( char val = *head; *head != '\0'; val = *(++head) )
    {
        if (! isNumeric( val ) )
            break;
        value *= 10;
        value += val - '0';
    }
    //
    return pushToken( eType::INT, value );
}

/**
    Munch an operator or special token
 */
static
sToken* lexer::munchSpecial( char* &stream )
{
    static sSymbol special[] =
    {
        { "*", eType::MUL    }, { "/", eType::DIV		},
        { "+", eType::ADD    }, { "-", eType::SUB		},
        { "(", eType::LPAREN }, { ")", eType::RPAREN	},
        { "&", eType::AND    }, { ",", eType::COMMA		},
        { "[", eType::LARRAY }, { "]", eType::RARRAY	},
        { "=", eType::EQU    }, { ";", eType::SEMICOLON	},
        { "{", eType::LBRACE }, { "}", eType::RBRACE    }
    };
    int nItems = sizeof( special ) / sizeof( sSymbol );

    /* check list to see if we can munch */
    for ( int i=0; i < nItems; i++ )
    {
        for ( int j=0; ;j++ )
        {
            if ( special[i].name[j] == '\0' )
            {
                stream += j;
                return pushToken( special[i].type, 0 );
            }
            if ( special[i].name[j] != stream[j] )
                break;
        }
    }

    /* sequence not munched */
    return nullptr;
}

/**
    Tokenize the input stream
 */
extern
bool lexer::consume( char *head )
{
    /*  */
    char *lastPos = 0;
    for ( ; *head != '\0' ; )
    {
        /* inform when stream may not progress */
        if ( lastPos == head )
            return error::complain( "Lexer has stalled" );
        lastPos = head;

        /* crunch input */
        if ( munchWhitespace( head ) ) continue;
        if ( munchLiteral   ( head ) ) continue;
        if ( munchSpecial   ( head ) ) continue;
        if ( munchSymbol    ( head ) ) continue;

        /* unknown token */
        return error::complain( "Unexpected character in source" );
    }
    
    /* push end symbol */
    pushToken( eType::END, 0 );

    return true;
}

/**
    Clear the lexer of all state
 */
extern
void lexer::reset( void )
{
    _lStack.clear( );

    _lineCounter	= 0;
    _tail			= 0;
}
