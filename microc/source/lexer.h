/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "global.h"

/**
    Source code tokenizer
 */
namespace lexer
{
    /**
        Tokenize the input string
     */
    extern bool consume( char *code );

    /**
        Get the number of tokens on the stack
     */
    extern int size( void );

    /**
        Pop a token off the token queue
     */
    extern sToken* pop( void );

    /**
        Peek the next token in the queue
     */
    extern sToken* peek( int la );

    /**
        Peek the type of the next token in the queue
     */
    extern eType_t peekType( int la );

    /**
        Release all state from the lexer
     */
    extern void reset( void );
};

