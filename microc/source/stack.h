/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#pragma once
#include "assert.h"

/**
    Generic stack/queue datastructure
 */
template <typename T, int nItems>
class cStack
{
private:

    /**
        The stack itself
     */
    T * list[ nItems ];

    /**
        List of stack pointers
     */
    int head;

public:
    
    /**
        Constructor
     */
    cStack( void )
        : head( 0 )
    { }
    
    /**
        Destructor
     */
    ~cStack( )
    { }
    
    /**
        get the number of items on the stack
     */
    int size( void )
    {
        return head;
    }

    /**
        push an item onto the stack
     */
    void push( T * item )
    {
        /* get current stack head */
        assert( head < nItems );

        list[ head++ ] = item;
    }
    
    /**
        pop the top item off the stack
     */
    T * pop( void )
    {
        assert( head > 0 );
        // grab this item off the stack
        T *out = list[ --head ];
        // scrub it
        list[ head ] = nullptr;
        return out;;
    }
    
    /**
        Discard n items from the top of the stacl
     */
    void discard( int n )
    {
        assert( head >= n );

        for ( int j=1; j<=n; j++ )
            list[ head-j ] = nullptr;
        head -= n;
    }

    /**
        Get an item relative to the top of the stack without removal
     */
    T * peek( int lb = 0 )
    {
        int ix = (head - 1) - lb;
        // check index is in range
        assert( ix >= 0 && ix < head );
        // grab this item off the stack
        return list[ ix ];
    }

    /**
        Index an item on the stack relative to the bottom
     */
    T * operator [] ( int index )
    {
        /* check index is in range */
        if ( index < 0 || index >= head )
            return nullptr;
        return list[ index ];
    }

    /**
        Remove all entries from the stack
     */
    void clear( void )
    {
        head = 0;
    }

};