/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "global.h"
#include "lexer.h"
#include "ast.h"
#include "assert.h"
#include "parser.h"
#include "allocator.h"
#include "error.h"

namespace parser
{
    
    /**
        Operator stack
     */
    static cStack<sToken,1024> oStack;

    // internal
    static int  prec				( const sToken *tok );
    static bool found				( const eType_t type );
    static void expect				( const eType_t type );

    static bool foundDataType		( void );

    static void enforce_precedence	( sToken *op, int & tide );

    static bool parse_MOD__			( void );
    static bool parse_VAL__			( void );
    static bool parse_EXP__			( int tide );

    static bool parse_decl			( void );
    static bool parse_expression	( void );
    static bool parse_statement		( void );
    static bool parse_function		( void );

    // interface
    extern bool consume				( void );
};

/**
    Get operator precedence or -1 when not an operator
 */
static
int parser::prec( const sToken *tok )
{
    assert( tok != nullptr );
    // higher number = higher need to bind
    switch ( tok->type )
    {
    default:		return -1;
        
    case ( '=' ):	return  0;
    case ( '+' ):	
    case ( '-' ):	return  1;
    case ( '*' ):	
    case ( '/' ):	return  2;
    }
}

/**
    Check if the next token is a data type specifier
 */
static
bool parser::foundDataType( void )
{
    assert( lexer::peek( 0 ) != nullptr );
    switch ( lexer::peek( 0 )->type )
    {
    default:
        return false;
        
    case ( eType::KEY_VOID ):
    case ( eType::KEY_INT  ):
        return true;

    }
}

/**
    Pop only if type matches the expected one
 */
static
bool parser::found( eType_t type )
{
    if ( lexer::peek( 0 )->type != type )
        return false;
    lexer::pop( );
    return true;
}

/**
    
 */
static
void parser::enforce_precedence( sToken *op, int & tide )
{
    /* pack to obey precedence rules */
    for ( ;oStack.size() > tide;  )
        if ( prec( oStack.peek( ) ) < prec( op ) )
            break;
        else
            ast::pack( oStack.pop(), 2 );
}

/**
    Parse an expression modifier
 */
static
bool parser::parse_MOD__( void )
{
    /* function call */
    if ( found( '(' ) )
    {
        int args = 0;
        if (! found( ')' ) )
        {
            do
            {
                /* parse an expression */
                if (! parse_expression( ) )
                    return false;
                args++;
            } 
            while ( found( ',' ) );
            /* expect param list closure */
            if (! found( ')' ) )
                return error::complain( "Expecting ')'" );
        }
        // 1 + args because we also need the name / function to call
        ast::pack( alloc::token( eType::FUNC ), 1 + args );
    }

    /* array index */
    else if ( found( '[' ) )
    {
        /* parse an array index expression */
        if (! parse_expression( ) )
            return false;
        /* pack as a dereference node */
        ast::pack( alloc::token( eType::DEREF ), 2 );
        /* close array index */
        if (! found( ']' ) )
            return error::complain( "Expecting ']'" );
    }

    /* success */
    return true;
}

/**
    Parse a value
 */
static
bool parser::parse_VAL__( void )
{
    /* pop LHS */
    sToken * tok = lexer::pop( );

    switch ( tok->type )
    {
    /* handle a literal / variable */
    case ( eType::SYM ):
    case ( eType::INT ):
        ast::push( tok );
        break;

    /* handle parenthesis */
    case ( '(' ):
        /* parse expression int parenthesis */
        if (! parse_expression( ) )
            return false;
        /* close parenthesis */
        if (! found( ')' ) )
            return error::complain( "Expecting ')'" );
        break;

    default:
        /* something is totaly wrong */
        return error::complain( "Unexpected token" );
    }

    /* parse modifiers */
    if (! parse_MOD__( ) )
        return false;

    /* success */
    return true;
}

/**
    Parse a sub expression
 */
static
bool parser::parse_EXP__( int tide )
{
    /* parse a value */
    if (! parse_VAL__( ) )
        return false;
    /* early exit when not an operator */
    if ( prec( lexer::peek( 0 ) ) == -1 )
        /* success */
        return true;
    /* get the operator */
    sToken * op = lexer::pop( );
    /* enforce precedence rules */
    enforce_precedence( op, tide );
    /* push this operator */
    oStack.push( op );
    /* parse RHS */
    return parse_EXP__( tide );
}

/**
    Parse an expression
 */
static
bool parser::parse_expression( void )
{
    /* mark the current place on the operator stack */
    int tide = oStack.size();

    /* expect a literal / symbol */
    /*XXX: ???? */

    /* parse an expression */
    if (! parse_EXP__( tide ) )
        return false;

    /* pack any left overs on the stack */
    while ( oStack.size( ) > tide )
        ast::pack( oStack.pop( ), 2 );

    /* encapsulate in expression node */
    ast::pack( alloc::token( eType::EXP, 0 ), 1 );

    /* success */
    return true;
}

/**
    Parse a variable declaration
 */
bool parser::parse_decl( void )
{
    int args = 2;

    /* push the data type */
    ast::push( lexer::pop( ) );
    
    /* pop the symbol name */
    sToken *sym = lexer::pop( );
    if ( sym->type != eType::SYM )
        return error::complain( "Expecting variable name" );
    ast::push( sym );

    /* array decl */
    if ( found( '[' ) )
    {
        /* parse array size */
        if (! parse_expression( ) )
            return false;
        // pack into array size
        args++;
        if (! found( ']' ) )
            return error::complain( "Expecting ']'" );
    }

    /* initalize with expression */
    if ( found( '=' ) )
    {
        if (! parse_expression( ) )
            return false;
        args++;
    }

    /* pack into decl node */
    ast::pack( alloc::token( eType::VAR_DECL, 0 ), args );
    /* terminator */
    if (! found( ';' ) )
        return error::complain( "Expecting ';'" );

    /* success */
    return true;
}

/**
    Parse a statement
 */
bool parser::parse_statement( void )
{
    /* find variable decs */
    if ( foundDataType( ) )
    {
        /* parse a variable declaration */
        if (! parse_decl( ) )
            return false;
    }
    else

    /* parse a block */
    if ( found( '{' ) )
    {
        int items = 0;
        while ( ! found( '}' ) )
        {
            /* parse statment in this block */
            if (! parse_statement( ) )
                return false;
            items++;
        }
        /* pack into a block */
        ast::pack( alloc::token( eType::BLOCK, 0 ), items );
    }

    /* parse an if statment */
    else if ( found( eType::KEY_IF ) )
    {
        /* parse if expression */
        if (! found( '(' ) )
            return error::complain( "Expecting '('" );
        parse_expression( );
        if (! found( ')' ) )
            return error::complain( "Expecting ')'" );
        /* parse if statment body */
        parse_statement( );
        /* parse asociated else statment */
        if ( found( eType::KEY_ELSE ) )
        {
            parse_statement( );
            ast::pack( alloc::token( eType::IF, 0 ), 3 );
        }
        else
            ast::pack( alloc::token( eType::IF, 0 ), 2 );
    }

    /* parse a while statment */
    else if ( found( eType::KEY_WHILE ) )
    {
        /* parse while statement expression */
        if (! found( '(' ) )
            return error::complain( "Expecting '('" );
        parse_expression( );
        if (! found( ')' ) )
            return error::complain( "Expecting ')'" );
        /* parse while statement body */
        if (! parse_statement( ) )
            return false;
        /* pack into while statment node */
        ast::pack( alloc::token( eType::WHILE, 0 ), 2 );
    }

    /* return statment */
    else if ( found( eType::KEY_RETURN ) )
    {
        /* parse return statement arguments */
        if (! found( ';' ))
        {
            parse_expression( );
            ast::pack( alloc::token( eType::RET, 0 ), 1 );
            if (! found( ';' ) )
                return error::complain( "Expecting ';'" );
        }
        else
            ast::push( alloc::token( eType::RET, 0 ) );
    }

    /* expression or function call */
    else
    {
        /* parse an expression */
        if (! parse_expression( ) )
            return false;
        /* close the expression */
        if (! found( ';' ) )
            return error::complain( "Expecting ';'" );
    }
    /* success */
    return true;
}

/**
    Parse and entire function
 */
bool parser::parse_function( void )
{
    /* parse the return type */
    if (! foundDataType( ) )
        return error::complain( "Expecting return type" );
    sToken *type = lexer::pop( );
    ast::push( type );

    /* function name */
    sToken *name = lexer::pop( );
    if ( name->type != eType::SYM )
        return error::complain( "Expecting function name" );
    ast::push( name );

    /* parse function arguments */
    int args = 0;
    if (! found( '(' ) )
        return error::complain( "Expecting arguments" );
    if (! found( ')' ) )
    {
        do 
        {
            /* get argument type */
            if (! foundDataType( ) )
                return error::complain( "Expecting argument data type" );
            sToken *type = lexer::pop( );
            ast::push( type );

            /* get argument name */
            sToken *sym = lexer::pop( );
            if ( sym->type != eType::SYM )
                return error::complain( "Expecting argument name" );
            ast::push( sym );

            /* add argument to list */
            ast::pack( alloc::token( eType::FUNC_ARG, 0 ), 2 );
            args++;
        }
        while ( found( ',' ) );

        /* end of argument list */
        if (! found( ')' ) )
            return error::complain( "Expecting ')' after arguments" );
    }

    /* parse function body */
    if (! parse_statement( ) )
        return false;

    /* pack into function decleration */
    ast::pack( alloc::token( eType::FUNC_DECL, 0 ), 3 + args );

    /* success */
    return true;
}

/**
    Consume all of source code
 */
bool parser::consume( void )
{
    /* parse all functions */
    int nFuncs = 0;
    do
    {
        if (! parse_function( ) )
            return false;
        nFuncs++;
    }
    while (! found( eType::END ) );

    /* pack functions into root */
    ast::pack( alloc::token( eType::ROOT, 0 ), nFuncs );

    return true;
}

/**
    Clear all state from the parser
 */
void parser::reset( void )
{
    oStack.clear( );
}