/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include <stdio.h>

#include "stream.h"
#include "fuzz.h"

#include "../../microc/source/microc.h"

static char *test =
"{													"
"    var1[ i + 1 ] = func( 1, 4+5, func2() ) * 2;	"
"    j = 3;											"
"    x = test( x );									"
"    if ( x )										"
"        x = test( x + 1 );							"
"	 else { x = 3; y = 2; }							"
"    return ;										"
"    return x+1;									"
"}													";

//
// 
//
int main( void )
{
    stream test;

    for ( int i=0; i<100000; i++ )
    {
        fuzz::generate( test );

        printf( "\n%s\n", test.ptr( ) );

        microc::compile( test.ptr( ) );
        
        test.reset( );

    }

    return 0;
}
