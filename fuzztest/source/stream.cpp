/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "stream.h"
#include <stdarg.h>
#include <stdio.h>
#include "assert.h"

stream::stream( void )
    : data    ( nullptr )
    , capacity( 2048 * 8 )
    , head    ( 0 )
{
    data = new char [ capacity ];
    assert( data != nullptr );
}

char * stream::ptr( void )
{
    return data;
}

void stream::printf( const char *str, ... )
{
    va_list args;
    va_start( args, str );

    int space   = capacity-head;
    int written = vsprintf_s( &data[ head ], space, str, args );

    assert( written >= 0 );
    assert( capacity > head+written );

    head += written;

    data[ head ] = '\0';
}

void stream::reset( void )
{
    head = 0;
    data[ 0 ] = '\0';
}
