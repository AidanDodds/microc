/*
 *	   _____   .__                        _________  
 *	  /     \  |__|  ____ _______   ____  \_   ___ \ 
 *	 /  \ /  \ |  |_/ ___\\_  __ \ /  _ \ /    \  \/ 
 *	/    Y    \|  |\  \___ |  | \/(  <_> )\     \____
 *	\____|__  /|__| \___  >|__|    \____/  \______  /
 *			\/          \/   Aidan Dodds 2014.    \/ 
 *
 */
#include "fuzz.h"
#include <stdlib.h>
#include <string.h>

namespace fuzz
{
    class gen;

    static int rand( int max );
};

int fuzz::rand( int max )
{
    return ::rand() % max;
}

class fuzz::gen
{
public:

    gen( stream & _str )
        : str( _str )
        , depth( 0 )
    {
        generate( );
    }

private:

    int      depth;
    stream & str;

    void gen_varname( void )
    {
//		if ( rand( 8 ) == 1 )
//			str.printf( "*" );

        for ( ;; )
        {
            char buf[8] = {0,0,0,0,0,0,0,0};

            for ( int i=0; i<1+rand(6); i++ )
                buf[i] = 'a'+rand(26);
    
            if ( strcmp( buf, "do"       ) == 0 ) continue;
            if ( strcmp( buf, "if"       ) == 0 ) continue;
            if ( strcmp( buf, "while"    ) == 0 ) continue;
            if ( strcmp( buf, "int"      ) == 0 ) continue;
            if ( strcmp( buf, "word"     ) == 0 ) continue;
            if ( strcmp( buf, "byte"     ) == 0 ) continue;
            if ( strcmp( buf, "void"     ) == 0 ) continue;
            if ( strcmp( buf, "return"   ) == 0 ) continue;
            if ( strcmp( buf, "continue" ) == 0 ) continue;
            if ( strcmp( buf, "break"    ) == 0 ) continue;
            if ( strcmp( buf, "else"     ) == 0 ) continue;
            if ( strcmp( buf, "null"     ) == 0 ) continue;
            if ( strcmp( buf, "for"      ) == 0 ) continue;

            str.printf( "%s", buf );
            break;
        }
    }

    void gen_literal( void )
    {
        char buf[8] = {0,0,0,0,0,0,0,0};
        for ( int i=0; i<1+rand(6); i++ )
            buf[i] = '0'+rand(9);
        str.printf( "%s", buf );
    }

    void gen_dataType( void )
    {
        switch ( rand(3) )
        {
        default:
            str.printf( "int " );
            break;
/*
        case ( 1 ):
            str.printf( "word " );
            break;
        case ( 2 ):
            str.printf( "byte " );
            break;
*/
        }
    }

    void gen_decl( void )
    {
        gen_dataType( );
        gen_varname( );

        if ( rand(10) < 5 )
        {
            str.printf( "=" );
            gen_expression( );
        }

        str.printf( ";" );
    }

    void gen_expression( void )
    {
        int pr = rand(2);

        if ( pr ) str.printf( "(" );

        if ( rand(2) == 0 )
        {
            if ( rand(2) == 0 )
                gen_varname( );
            else
                gen_literal( );
        }
        else
        {
            gen_expression( );
            
            switch ( rand( 3 ) )
            {
            case ( 0 ): str.printf( "+" ); break;
            case ( 1 ): str.printf( "-" ); break;
            case ( 2 ): str.printf( "*" ); break;
            }
            if ( rand(2) == 0 )
                gen_varname( );
            else
                gen_literal( );
        }

        if ( pr ) str.printf( ")" );
    }

    void gen_return( void )
    {
        str.printf( "return " );

        if ( rand( 2 ) == 1 )
        {
            str.printf( " " );
            gen_expression( );
        }

        str.printf( ";" );
    }

    void gen_block( void )
    {
        str.printf( "{" );

        for ( int i=rand(10); i>0; i-- )
            gen_statement( );

        str.printf( "}" );
    }

    void gen_assign( void )
    {
        gen_varname( );
        str.printf( "=" );
        gen_expression( );
        str.printf( ";" );
    }

    void gen_statement( void )
    {
        depth++;

        if ( depth >= 3 )
            gen_assign( );
        else
        {
            switch ( rand( 8 ) )
            {
            case ( 0 ):	gen_assign( ); break;
            case ( 1 ):	gen_assign( ); break;
            case ( 2 ):	gen_assign( ); break;
            case ( 3 ): gen_return( ); break;
            case ( 4 ): gen_decl  ( ); break;
            case ( 5 ): gen_block ( ); break;
            case ( 6 ): gen_if    ( ); break;
            case ( 7 ): gen_while ( ); break;
            }
        }

        depth--;
    }

    void gen_if( void )
    {
        str.printf( "if (" );
        gen_expression( );
        str.printf( ")" );
        if ( rand(2) == 1 )	gen_block( );
        else				gen_statement( );
    }

    void gen_while( void )
    {
        str.printf( "while (" );
        gen_expression( );
        str.printf( ")" );
        if ( rand(2) == 1 )	gen_block( );
        else				gen_statement( );
    }

    void gen_function( void )
    {
        gen_dataType( );
        gen_varname( );
        str.printf( "(" );
        for ( int i=rand(4); i>=0; --i )
        {
            gen_dataType( );
            gen_varname( );
            if ( i > 0 )
                str.printf( ", " );
        }
        str.printf( ")" );
        gen_block( );
        str.printf( "\n" );
    }

    void generate( void )
    {
        gen_function( );
    }
    
};

void fuzz::generate( stream &str )
{
    gen myGen( str );
}