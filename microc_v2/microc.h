#pragma once 

#include "lexer.h"
#include "parser.h"
#include "error.h"
#include "alloc.h"

class cMicroc
{
    cAlloc  alloc;
    cError  error;
    cLexer  lexer;
    cParser parser;

public:

    /**
        Constructor
     */
    cMicroc( )
        : error( )
        , lexer ( error, alloc )
        , parser( error, lexer, alloc )
    {}

    /**
        Compile an AST tree from the lexer stream
     */
    bool compile( const char *source )
    {
        error.clear( );
        /* set the lexer source stream */
        lexer.setSource( source );
        /* build an AST tree */
        if (! parser.parse( ) )
            return false;
        /* codegen from the AST tree */

        /* success */
        return true;
    }

};