#pragma once

#include "error.h"
#include "node.h"
#include "alloc.h"

class cLexer
{
    cError & error;
    cAlloc & alloc;

public:

    /**
        Constructor
     */
    cLexer( cError & err, cAlloc & all )
        : error( err )
        , alloc( all )
    {}

    /**
        Set the source code stream
     */
    bool setSource( const char *source );

    /**
        Lex a new node and return it
     */
    sNode * pop( void );

};