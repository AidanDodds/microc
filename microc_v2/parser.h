#pragma once

#include "error.h"
#include "lexer.h"

class cParser
{
    cError & error;
    cLexer & lexer;
    cAlloc & alloc;

public:

    /**
        Constructor
     */
    cParser( cError & err, cLexer &lex, cAlloc &all )
        : error( err )
        , lexer( lex )
        , alloc( all )
    {}

    /**
        Begin parsing
     */
    bool parse( void );

};