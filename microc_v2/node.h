#pragma once

namespace node
{

/**
    Node Types
 */
enum eType
{
    
    /**
        Lexer Token Types
     */

    TEnd         = 0x00,

    /* operator tokens */
    TLComma      = ','   ,
    TLQuote      = '\"'  ,
    TLDot        = '.'   ,
    TLEquals     = '='   ,
    TLStar       = '*'   ,
    TLSlash      = '/'   ,
    TLLParen     = '('   ,
    TLRParen     = ')'   ,
    TLLBrace     = '['   ,
    TLRBrace     = ']'   ,
    TLLArrow     = '<'   ,
    TLRArrow     = '>'   ,
    TLHash       = '#'   ,
    TLNot        = '!'   ,
    TLAnd        = '&'   ,
    TLPipe       = '|'   ,
    TLPercent    = '%'   ,

    /* symbol and literals */
    TLInteger    = 0x80  ,
    TLSymbol     ,
    TLFloat      ,
    TLString     ,

    /**
        composite AST types
     */

    /* expression */
    TAExpr       = 0x100,

    /* expression subtypes */
    TAAdd        ,          // '+'
    TASub        ,          // '-'
    TAMul        ,          // '*'
    TADiv        ,          // '/'
    TAMod        ,          // '%'
    TACmpLT      ,          // '<'
    TACmpGT      ,          // '>'
    TACmpEQ      ,          // '=='
    TALTEQ       ,          // '<='
    TAGTEQ       ,          // '>='
    TANotEQ      ,          // '!='
    TANot        ,          // '!'
    TAAnd        ,          // '&'
    TAAccess     ,          // '.'
    TAOr         ,          // '|'

    TAAssign     ,          // [TLSymbol] '=' [TAExpr]
    TACall       ,          // [TLSymbol] '(' < [TAExpr] * > ')'
    TAIndex      ,          // [TLSymbol] '[' [TAExpr] ']'

    /* statement */
    TAStmt       ,
   
    /* statment sybtypes */
    TAIf         ,          // 'if'    '(' [TAExpr] ')' [TAStmt]
    TAWhile      ,          // 'while' '(' [TAExpr] ')' [TAStmt]
    TABlock      ,          // '{' [TAStmt]* '}'
    TADeclVar    ,          // [TLSymbol] [TLSymbol] < '=' [TAExpr] > ';'
    TABreak      ,          // 'break' ';'
    TAReturn     ,          // 'return' [TAExpr] ';'
    TARunExpr    ,          // [TAExpr] ';'

    /* function decl */
    TADeclFunc   ,          // [TLSymbol] [TLSymbol] '(' < < [TADeclArg] ',' > [TADeclArg] > ')' [TAStmt]
    TADeclArg    ,          // [TLSymbol] [TAAssign]

}; /* enum eType */

}; /* namespace node */

/**

 */
struct sValue
{
    sValue( void )
    {
        data.ivalue = 0;
    }

    union
    {
        float fvalue;
        int   ivalue;
        char *string[2];
    }
    data;
}; /* class sValue */

/**
	A composite token / AST node
 */
struct sNode
{
private:

	/* sibling of this node */
	sNode* 	next;
	
	/* index to child */
	sNode* 	child;

    /* origin source line number */
    short lineNum;

public:
	
	/* node type */
	enum node::eType type;
	
	/* value for this type */
	sValue value;
	
	/*	constructor */
	sNode( void )
		: child( nullptr    )
		, next ( nullptr    )
        , type ( node::TEnd )
        , value(            )
	{ }
	
	/* add a child to this node */
	void pushChild( sNode * n )
	{
		n->next = child;
		child = n;
	}
	
	/* get a child */
	sNode * getChild( int index )
	{
		sNode * c = child;
		
		while ( (index-->0) && (c!=nullptr) )
			c = c->next;
			
		return c;
	}
	
    /* type check */
    bool isA( enum eType a )
    {
        return a == type;
    }

    /* check if two nodes are the same */
    bool matches( const sNode & node );

    /* get specific node data */
    bool get( float &out );
    bool get( int   &out );
    bool get( char *dst, int max );

}; /* struct sNode */
