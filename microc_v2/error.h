#pragma once

class cError
{
public:

    /**
        Set an error state
     */
    void set( const char *msg );

    /**
        Clear an error state
     */
    void clear( void );

    /**
        Check if an error state is set
     */
    bool isSet( void );

    /**
        Get the last error string
     */
    const char *get( void );

};